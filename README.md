# nds-toolchain-conan

This repo contains several conan packages for compiling nds applications.

`~/.conan/settings.yml` should include the architectures `armv5te` and `armv7tdmi` as
well as the "operating system" `NDS`.
import os
from conans import ConanFile

class Libfat(ConanFile):
    name = 'libfat'
    version = 'v4'
    url = 'https://gitlab.com/monlib/nds-toolchain-conan.git'
    homepage = 'https://sourceforge.net/projects/devkitpro/files/libfat/'
    settings = {'os': ['NDS'], 'arch': ['armv5te'], 'compiler': ['gcc']}
    description = 'libfat link flags. Does not install anything.'
    requires = 'libnds/v3@monlib/stable'

    def package_info(self):
        self.cpp_info.libs = ['fat']
        self.cpp_info.includedirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'include')]
        self.cpp_info.libdirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'lib')]

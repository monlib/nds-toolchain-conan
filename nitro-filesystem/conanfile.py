import os
from conans import ConanFile

class NitroFilesystem(ConanFile):
    name = 'nitro-filesystem'
    version = 'v4'
    url = 'https://gitlab.com/monlib/nds-toolchain-conan.git'
    homepage = 'https://sourceforge.net/projects/devkitpro/files/filesystem/'
    settings = {'os': ['NDS'], 'arch': ['armv5te'], 'compiler': ['gcc']}
    description = 'nitroFS link flags. Does not install anything.'
    requires = (
        'libnds/v3@monlib/stable',
        'libfat/v4@monlib/stable',
    )

    def package_info(self):
        self.cpp_info.libs = ['filesystem']
        self.cpp_info.includedirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'include')]
        self.cpp_info.libdirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'lib')]

from conans import ConanFile

class DsTarget(ConanFile):
    name = 'ds-target'
    version = '0.2.0'
    license = 'MIT'
    url = 'https://gitlab.com/monlib/nds-toolchain-conan.git'
    settings = {'os': ['NDS'], 'arch': ['armv5te', 'armv7tdmi'], 'compiler': ['gcc']}
    description = """Defines flags for compiling libraries for nds homebrew.
        This conan package defines environment variables and flags for compiling libraries for the nds.
        It supports both arm7 and arm9 libraries, but not executables. Because this package is meant to
        compile platform agnostic libraries, it does not link against libnds."""

    def package_info(self):
        if self.settings.arch == 'armv5te':
            self.cpp_info.defines = ['ARM9']

            # These flags were copied from the template makefile.
            # -Wall was omitted due to it possibly breaking compiles (if something sets -Werror).
            self.cpp_info.cflags = [
                '-march=armv5te',
                '-mtune=arm946e-s',
                '-fomit-frame-pointer',
                '-ffast-math',
                '-mthumb',
                '-mthumb-interwork'
            ]

            # -fno-exceptions is omitted because it most likely breaks compiles and isn't needed.
            # The same applies to -fno-rtti.
            self.cpp_info.cppflags.extend(self.cpp_info.cflags)

            self.cpp_info.exelinkflags = [
                '-specs=ds_arm9.specs', '-g', '-Wl,-Map,.map', '-mthumb', '-mthumb-interwork'
            ]

        elif self.settings.arch == 'armv7tdmi':
            self.cpp_info.defines = ['ARM7']

            # See the comments in the ARM9 section.
            self.cpp_info.cflags = [
                '-mcpu=arm7tdmi',
                '-mtune=arm7tdmi',
                '-fomit-frame-pointer',
                '-ffast-math',
                '-mthumb-interwork'
            ]

            self.cpp_info.cppflags.extend(self.cpp_info.cflags)

            self.cpp_info.exelinkflags = [
                '-specs=ds_arm7.specs', '-g', '-mthumb-interwork', '-Wl,--nmagic', '-Wl,-Map,.map'
            ]

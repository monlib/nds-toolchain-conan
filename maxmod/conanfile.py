import os
from conans import ConanFile

class Maxmod(ConanFile):
    name = 'maxmod'
    version = 'v3'
    url = 'https://gitlab.com/monlib/nds-toolchain-conan.git'
    homepage = 'https://sourceforge.net/projects/devkitpro/files/maxmod/'
    settings = {'os': ['NDS'], 'arch': ['armv5te', 'armv7tdmi'], 'compiler': ['gcc']}
    description = 'maxmod link flags. Does not install anything.'
    license = 'maxmod_license.txt'

    def package_info(self):
        self.cpp_info.libs = ['mm9' if self.settings.arch == 'armv5te' else 'mm7']
        self.cpp_info.includedirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'include')]
        self.cpp_info.libdirs = [os.path.join(os.environ['DEVKITPRO'], 'libnds', 'lib')]
